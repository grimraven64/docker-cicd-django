FROM python:3-alpine

# создаем папку для файлов приложения
WORKDIR /code 

# для того, чтобы не устанавливать зависимости при каждой сборке, копируем только файл с зависимостями (это экономит время)
COPY ./requirements.txt /code
RUN pip install -r requirements.txt
RUN mkdir db
# копируем файлы приложения в созданную папку
COPY . /code

# объявляем порт приложения
EXPOSE 8000
CMD sh init.sh && python manage.py runserver 0.0.0.0:8000